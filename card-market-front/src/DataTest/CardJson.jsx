export const CardJson = {
    cards:[
        {
            id:1,
            name:"card1",
            description:"card1",
            family:"family_fire",
            affinity:"fire",
            energy:50,
            hp:25,
            price:12.5,
            image:"",
            legend: "Carte de feu"
        },
        {
            id:2,
            name:"card2",
            description:"card2",
            family:"family_water",
            affinity:"water",
            energy:75,
            hp:20,
            price:13.5,
            image:"",
            legend: "Carte d'eau"
        }
    ]
}