import {Container, Table} from 'react-bootstrap'
import {Card} from '../Models/Card'

export function CardsTable(props){
    // recuperation de la data
    if(props.cardsList.length > 0){
        return(
            <Container className='CardsList' >
                <h3>ListCard</h3>
                <Table hover responsive >
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Family</th>
                            <th>Affinity</th>
                            <th>Energy</th>
                            <th>HP</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.cardsList.map((cardItem) => <Card key={cardItem.id} card={cardItem} display_type={"TABLE"}/>)}
                    </tbody>
                </Table>
            </Container>
        )
    }else{
        return(<Table/>)
    }
}