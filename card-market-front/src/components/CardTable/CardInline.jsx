import {useDispatch} from "react-redux";
import {setSelectedCard} from "../../core/actions/cardActions";

export function CardInline(props){
    const dispatch = useDispatch()
    return (
        <tr onClick={() => dispatch(setSelectedCard(props.cardItem))}>
            <td>{props.cardItem.name}</td>
            <td>{props.cardItem.description}</td>
            <td>{props.cardItem.family}</td>
            <td>{props.cardItem.affinity}</td>
            <td>{props.cardItem.energy}</td>
            <td>{props.cardItem.hp}</td>
            <td>{props.cardItem.price}</td>
        </tr>
    )
}