import {GET_USER_NAME, GET_WALLET_AMOUNT, User} from "../Models/User";
import {Badge, Col, Container, Image, OverlayTrigger, Row, Tooltip} from "react-bootstrap";
import profile from "../../img/profil.png"
import {useSelector} from "react-redux";
import {SelectCurrentUser} from "../../core/selector/userSelector";

export const Header = (props) => {
    const currentUser = useSelector(SelectCurrentUser);
    if (!(currentUser===undefined)){
        return( 
            <Container className={"headerWeb"}>
                <Row>
                    <Col xs={3} md={2} lg={2}>
                        <OverlayTrigger
                            placement="right"
                            delay={{ show: 250, hide: 400 }}
                            overlay={renderMoneyTooltip}
                        >
                            <Badge pill bg="success" className="pillMoney">
                                <User display_type = {GET_WALLET_AMOUNT} user = {currentUser}/> €
                            </Badge>
                        </OverlayTrigger>

                    </Col>
                    <Col xs={3} md={6} lg={8}>
                        <h1><b> {props.title} </b></h1>
                    </Col>
                    <Col id="profileDisplay" xs={3} md={4} lg={2}>

                        <OverlayTrigger
                            placement="left"
                            delay={{ show: 250, hide: 400 }}
                            overlay={renderProfileTooltip(currentUser)}
                        >
                            <Badge pill className="pillProfile">
                                <Image src={profile} className="icons" width="15px" height="15px"/>
                                <User display_type = {GET_USER_NAME} user = {currentUser}/>
                            </Badge>
                        </OverlayTrigger>
                    </Col>
                </Row>
            </Container>
        )
    }else{
        return(
            <Container className={"headerWeb"}>
                <Row>
                    <Col xs={3} md={2} lg={2}/>
                    <Col xs={3} md={6} lg={8}>
                        <h1><b> {props.title} </b></h1>
                    </Col>
                    <Col xs={3} md={2} lg={1}/>
                    <Col xs={3} md={2} lg={1}/>
                </Row>
            </Container>
        )
    }
}

const renderMoneyTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
        Votre argent disponible
    </Tooltip>
);

const renderProfileTooltip = (props) => (
    <Tooltip id="button-tooltip" {...props}>
        Actuellement connecté en tant que <b><User display_type = {GET_USER_NAME} user = {props}/></b>
    </Tooltip>
);