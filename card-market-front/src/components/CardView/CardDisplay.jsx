import {Card, Row, Col, Container} from 'react-bootstrap';
import {Image} from 'react-bootstrap';
import {useSelector} from "react-redux";
import {SelectCardSelected} from "../../core/selector/cardSelector";

export function CardDisplay(){
    const cardSelected = useSelector(SelectCardSelected);
    return (
        <Container>
            <Card>
                <Row>
                    <Col>
                        <Image/>{cardSelected.energy}
                    </Col>
                    <Col>
                        <h3>{cardSelected.family}</h3>
                    </Col>
                    <Col>
                        {cardSelected.hp}<Image/>
                    </Col>
                </Row>
                <Card.Img variant="top" src={cardSelected.image} />
                <Card.Body>
                    <Card.Title>{cardSelected.name}</Card.Title>
                    <Card.Text>
                        {cardSelected.legend}
                    </Card.Text>
                </Card.Body>
            </Card>
        </Container>
    )
}