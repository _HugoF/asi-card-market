import {useDispatch, useSelector} from "react-redux";
import {SelectWindowsSelected} from "../core/selector/navigatorSelector";
import {
    LOGIN_WINDOWS,
    MENU_WINDOWS,
    PLAY_WINDOWS,
    SELL_WINDOWS,
    SHOP_WINDOWS,
    setCurrentInternWindows
} from "../core/actions/navigatoryActions";
import {Button, Container} from "react-bootstrap";
import {LogIn} from "./InternWindows/LogIn";
import {SignIn} from "./InternWindows/SignIn";
import {Menu} from "./InternWindows/Menu";
import {Sell} from "./InternWindows/Sell";


export const InternWindows = () =>{
    const name = useSelector(SelectWindowsSelected);
    const dispatch = useDispatch()
    switch (name){
        case PLAY_WINDOWS[0]:
            return (
                <Container className={"PlayWindows"}>
                    <Button size="lg" onClick={()=>dispatch(setCurrentInternWindows(MENU_WINDOWS))}>Retour au menu</Button>
                </Container>
            );
        case SHOP_WINDOWS[0]:
            return (
                <Sell onShop ={true}/>
            );
        case SELL_WINDOWS[0]:
            return (
                <Sell onShop = {false}/>
            );
        case LOGIN_WINDOWS[0]:
            return (
                <LogIn/>
            );
        case MENU_WINDOWS[0]:
            return (
                <Menu/>
            );
        default:
            return(
                <SignIn/>
            );

    }
}