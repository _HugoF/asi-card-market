export const GET_WALLET_AMOUNT = 'AMOUNT_OF_MONEY_IN_THE_USER_WALLET'
export const GET_USER_NAME = 'USER_NAME'

export const User = (props) => {
    switch (props.display_type){
        case GET_WALLET_AMOUNT:
            return (props.user.wallet);
        case GET_USER_NAME:
            return (props.user.name);
        default:
            return null;
    }
}