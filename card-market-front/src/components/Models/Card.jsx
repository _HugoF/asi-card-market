import {CardDisplay} from '../CardView/CardDisplay'
import {CardInline} from '../CardTable/CardInline'
export function Card(props){
    if (props.display_type === 'TABLE'){
        return(
            <CardInline cardItem = {props.card} OnCardSelected={props.OnCardSelected}/>
        );
    }else{
        return(
            <CardDisplay cardItem = {props.card}/>
        );
    }
}