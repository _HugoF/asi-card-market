import {Button, Col, Container, Row} from "react-bootstrap";
import {CardsTable} from "../CardTable/CardsTable";
import {CardDisplay} from "../CardView/CardDisplay";
import {UseData} from "../../hooks/GetData";
import {useNavigate} from "react-router";
import {useDispatch, useSelector} from "react-redux";
import {SELL_WINDOWS, setCurrentInternWindows, SHOP_WINDOWS} from "../../core/actions/navigatoryActions";
import {Header} from "../Header/Header";
import {SelectTitleWindowsSelected} from "../../core/selector/navigatorSelector";

export const Sell = (props) => {
    const cardsList = UseData("Card");
    const navigate = useNavigate();
    const dispatch = useDispatch();
    (props.onShop?dispatch(setCurrentInternWindows(SHOP_WINDOWS)):dispatch(setCurrentInternWindows(SELL_WINDOWS)));
    return(
        <Container className={"SellWindows"}>
            <Row>
                <Header title = {useSelector(SelectTitleWindowsSelected)}/>
            </Row>
            <Row>
                <Button size="sm" onClick={()=>navigate('/menu')}>PREVIOUS</Button>
            </Row>
            <Row>
                <Col xs={4} md={6} lg={8}><CardsTable cardsList={cardsList}/></Col>
                <Col xs={8} md={6} lg={4}>
                    <Row>
                        <CardDisplay/>
                    </Row>
                    <Button>{props.onShop ? 'BUY':'SELL'}</Button>
                </Col>
            </Row>
        </Container>
    );
}