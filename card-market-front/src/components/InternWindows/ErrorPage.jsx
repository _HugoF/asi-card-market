import {useDispatch, useSelector} from "react-redux";
import {ERROR_WINDOWS, setCurrentInternWindows} from "../../core/actions/navigatoryActions";
import {Header} from "../Header/Header";
import {Container, Row} from "react-bootstrap";
import {SelectTitleWindowsSelected} from "../../core/selector/navigatorSelector";

export const ErrorPage = () => {
    const dispatch = useDispatch();
    dispatch(setCurrentInternWindows(ERROR_WINDOWS));
    return(
        <Container className={'ErrorWindows'}>
            <Row>
                <Header title = {useSelector(SelectTitleWindowsSelected)}/>
            </Row>
            <p>Tu t'es perdu mon pote</p>
        </Container>
    );
}