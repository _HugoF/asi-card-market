import {Button, Container, Form, Image, Row} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {UseData} from "../../hooks/GetData";
import {setSelectedUser} from "../../core/actions/userActions";
import down from "../../img/down-chevron.png"
import login from "../../img/log-in.png"
import signin from "../../img/sign-in.png"
import {useNavigate} from "react-router";
import {LOGIN_WINDOWS, setCurrentInternWindows} from "../../core/actions/navigatoryActions";
import {Header} from "../Header/Header";
import {SelectTitleWindowsSelected} from "../../core/selector/navigatorSelector";

let nom = "";
let password = "";
let currentUser;

export const LogIn = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    dispatch(setCurrentInternWindows(LOGIN_WINDOWS));
    return(
        <Container className={"LoginWindows"}>
            <Row>
                <Header title = {useSelector(SelectTitleWindowsSelected)}/>
            </Row>
            <span className="infoTitre">
                Prêt pour une <b>nouvelle partie</b> ?
            </span>
            <Form>
                <Form.Group className="mb-3">
                    <Form.Label><b>Identifiant</b></Form.Label>
                    <Form.Control
                        placeholder="Insérez ici votre nom..."
                        aria-label="Nom"
                        onChange={e => nom =(e.target.value)}
                    />
                    <Form.Text className="text-muted">
                        Votre identifiant de connexion est votre <b>nom de famille</b>
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label><b>Mot de passe</b></Form.Label>
                    <Form.Control
                        placeholder="Insérez ici votre mot de passe..."
                        type="password"
                        aria-label="Password"
                        onChange={e => password =(e.target.value)}
                    />
                </Form.Group>
                <div className="buttonsLogIn">
                    <Button size="mg" variant="success" onClick = { () =>{
                        if(LogInApplication()){
                            dispatch(setSelectedUser(currentUser));
                            navigate('/menu');
                        }
                    }}>
                        <Image className="icons" alt="log in icon" src={login} height="16px"/>
                        Connexion
                    </Button>
                    <br />
                    <br />

                    <Form.Text className="text-muted">
                        <Image className="icons" alt="down arrow" src={down} height="16px"/>
                        Vous n'avez toujours pas de compte ? <b>Inscrivez-vous ci dessous</b>
                        <Image className="icons" alt="down arrow" src={down} height="16px"/>
                    </Form.Text>

                    <br />

                    <Button size="mg" onClick={()=>navigate('/signin')}>
                        <Image className="icons" alt="sign in" src={signin} height="16px" />
                        Inscription
                    </Button>

                </div>
            </Form>
        </Container>
    );

}

const LogInApplication = () => {
    if (checkLogIn()) {
        // Récupération de l'utilisateur via la réponse de l'API
        currentUser = UseData('User');
        // Mise à jour de l'utilisateur dans le store
        console.log("Utilisateur " + currentUser.name + " connecté");
        return true;
    } else {
        console.error("Erreur lors de la connexion de l'utilisateur");
        return false;
    }
}

const checkLogIn = () => {
    if(password) {
        // TODO
    }
    if(nom) {
        // TODO
    }
    return true;
}

