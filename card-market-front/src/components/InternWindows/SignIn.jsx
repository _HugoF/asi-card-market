import {Button, Container, Form, Image, Row} from "react-bootstrap";
import signin from "../../img/sign-in.png";
import back from "../../img/back.png";
import {useNavigate} from "react-router";
import {useDispatch, useSelector} from "react-redux";
import {INSCRIPTION_WINDOWS, setCurrentInternWindows} from "../../core/actions/navigatoryActions";
import {Header} from "../Header/Header";
import {SelectTitleWindowsSelected} from "../../core/selector/navigatorSelector";

let prenom = "";
let nom = "";
let password = "";
let repassword = "";

export const SignIn = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    dispatch(setCurrentInternWindows(INSCRIPTION_WINDOWS));
    return(
        <Container className={"InscriptionWindows"}>
            <Row>
                <Header title = {useSelector(SelectTitleWindowsSelected)}/>
            </Row>
            <span className="infoTitre">
                Prêt à <b>rejoindre l'équipe</b> ?
            </span>
            <Row>
                <Form>
                    <Form.Group className="mb-3">
                        <Form.Label><b>Prénom</b><span className="mandatoryField">*</span></Form.Label>
                        <Form.Control
                            placeholder="Insérez ici votre prénom..."
                            aria-label="Prénom"
                            onChange={e => prenom =(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label><b>Nom</b><span className="mandatoryField">*</span></Form.Label>
                        <Form.Control
                            placeholder="Insérez ici votre nom..."
                            aria-label="Nom"
                            onChange={e => nom =(e.target.value)}
                        />
                        <Form.Text className="text-muted">
                            Votre nom de famille sera votre <b>identifiant de connexion</b>
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="passwordLabelSignIn">
                        <Form.Label><b>Mot de passe</b><span className="mandatoryField">*</span></Form.Label>
                        <Form.Control
                            placeholder="Saissisez votre mot de passe..."
                            type="password"
                            aria-label="Password"
                            onChange={e => password =(e.target.value)}
                        />
                    </Form.Group>

                    <Form.Group className="passwordLabelSignIn">
                        <Form.Control
                            placeholder="Saissisez à nouveau votre mot de passe..."
                            type="password"
                            aria-label="Repassword"
                            onChange={e => repassword =(e.target.value)}
                        />
                    </Form.Group>

                    <span className="mandatoryFields">
                        * Champs obligatoires
                    </span>

                    <br />
                    <Button size="mg" variant="success" onClick = {() => SignInApplication() ? navigate('/') : navigate('/signin')}>
                        <Image className="icons" alt="sign in" src={signin} height="16px"/> S'inscrire
                    </Button>

                    <br />
                    <br />
                    <Button onClick={()=>navigate('/')}>
                        <Image className="icons" alt="go back to log in icon" src={back} height="16px" />Retour à la page de connexion
                    </Button>

                </Form>
            </Row>
        </Container>
    );
}

const SignInApplication = () => {
    if (checkSignIn()) {
        console.log("Utilisateur " + prenom + " " + nom + " créé");
        return true;
    } else {
        console.error("Erreur lors de la création de l'utilisateur");
        return false;
    }

}

const checkSignIn = () => {
    if (password === repassword) {
        return true;
    }
    return false;
}
