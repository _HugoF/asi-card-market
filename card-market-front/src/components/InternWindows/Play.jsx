import {Button, Col, Container, Row} from "react-bootstrap";
import {useNavigate} from "react-router";
import {useDispatch, useSelector} from "react-redux";
import {PLAY_WINDOWS, setCurrentInternWindows} from "../../core/actions/navigatoryActions";
import {Header} from "../Header/Header";
import {Chat} from "../Chat/Chat";
import {SelectTitleWindowsSelected} from "../../core/selector/navigatorSelector";

export const Play = () =>{
    const navigate = useNavigate();
    const dispatch = useDispatch();
    dispatch(setCurrentInternWindows(PLAY_WINDOWS));
    return (
        <Container className={"PlayWindows"}>
            <Row>
                <Header title = {useSelector(SelectTitleWindowsSelected)}/>
            </Row>
            <Row className="mainContentPlay">
                <Col xs={3} md={3} lg={3} className="chat">
                    <Chat/>
                </Col>
                <Col xs={9} md={9} lg={9} className="currentGame">
                
                
                </Col>
            </Row>

            <br />

            <Row className="backMenuButtonRow">
                <Button size="mg" className="backMenuCurrentButton" onClick={()=>navigate('/menu')}>Retour au menu</Button>
            </Row>
        </Container>
    );
}
