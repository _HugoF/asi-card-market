import {Button, Col, Container, Image, Row} from "react-bootstrap";
import hand from "../../img/hand-shake.png"
import shopping from "../../img/shopping-cart.png"
import play from "../../img/play-button.png"
import {useNavigate} from "react-router";
import {MENU_WINDOWS, setCurrentInternWindows} from "../../core/actions/navigatoryActions";
import {useDispatch, useSelector} from "react-redux";
import {Header} from "../Header/Header";
import {SelectTitleWindowsSelected} from "../../core/selector/navigatorSelector";


export const Menu = ()=>{
    const navigate = useNavigate();
    const dispatch = useDispatch();
    dispatch(setCurrentInternWindows(MENU_WINDOWS));
    return(
        <Container className={"Menu"}>
            <Row>
                <Header title = {useSelector(SelectTitleWindowsSelected)}/>
            </Row>
            <span className="infoTitre">
                Bien plus qu'un <b>jeu de carte</b>...
            </span>
            <div className="itemsMenu">
                <Row>
                    <Col>

                        <Button className="items" onClick={()=>navigate('/sell')}>
                            <Image className="iconsMenu" alt="log in icon" src={hand} height="40px"/>
                            VENDRE
                        </Button>

                    </Col>
                    <Col>
                        <Button className="items" onClick={()=>navigate('/buy')}>
                            <Image className="iconsMenu" alt="log in icon" src={shopping} height="40px"/>
                            ACHETER
                        </Button>
                    </Col>
                </Row>

                <br />

                <Row>
                    <Col>
                        <Button className="items" onClick={()=>navigate('/play')}>
                            <Image className="iconsMenu" alt="log in icon" src={play} height="40px"/>
                            JOUER
                        </Button>
                    </Col>
                </Row>
            </div>
        </Container>
    );
}