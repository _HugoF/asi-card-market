import {CardJson} from "../DataTest/CardJson";
import {UsersJson} from "../DataTest/UsersJson";
import {UserJson} from "../DataTest/UserJson";


export const UseData = (url) => {
    // Uncomment when we have API
    //
    //const [list,SetList] = useState([])
    // useEffect( async () => {
    //     const result = await fetch(url)
    //     const listResult = await result.json()
    //     SetList(listResult)
    // },[])
    //return list;

    // For testing
    switch (url){
        case 'Card':
            return CardJson.cards;
        case 'User':
            return UserJson;
        default:
            return UsersJson.users;
    }
}