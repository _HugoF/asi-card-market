import {useDispatch} from "react-redux";

export function Tools(action){
    const dispatch = useDispatch();
    return (
        <div>{dispatch(action)}</div>
    );

}