import './App.css';
import {Container} from "react-bootstrap";
import {BrowserRouter, Route} from "react-router-dom";
import {LogIn} from "./components/InternWindows/LogIn";
import {Sell} from "./components/InternWindows/Sell";
import {Menu} from "./components/InternWindows/Menu";
import {SignIn} from "./components/InternWindows/SignIn";
import {Play} from "./components/InternWindows/Play";
import {ErrorPage} from "./components/InternWindows/ErrorPage";
import {useSelector} from "react-redux";
import {SelectCurrentUser} from "./core/selector/userSelector";
import {Navigate, Routes} from "react-router";


export function App() {
    const currentUser = useSelector(SelectCurrentUser);
    return (
        <Container className="App">
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<LogIn/>}/>
                    <Route path="signin" element={<SignIn/>}/>
                    <Route path="menu" element={currentUser===undefined?<Navigate to ="/" />:<Menu/>}/>
                    <Route path="play" element={currentUser===undefined?<Navigate to ="/" />:<Play/>}/>
                    <Route path="sell" element={currentUser===undefined?<Navigate to ="/" />:<Sell onShop = {false}/>}/>
                    <Route path="buy" element={currentUser===undefined?<Navigate to ="/" />:<Sell onShop = {true}/>}/>
                    <Route path="*" element={<ErrorPage/>}/>
                </Routes>
            </BrowserRouter>
        </Container>
    );
}
