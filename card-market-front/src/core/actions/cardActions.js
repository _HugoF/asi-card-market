export const UPDATE_ID_CARD = 'UPDATE_SELECTED_ID_CARD'
export const UPDATE_CARD = 'UPDATE_SELECTED_CARD'
export function setSelectedCardId(idCard){
    return {
        type: UPDATE_ID_CARD,
        payload: idCard
    }
}
export function setSelectedCard(card){
    return {
        type: UPDATE_CARD,
        payload: card
    }
}