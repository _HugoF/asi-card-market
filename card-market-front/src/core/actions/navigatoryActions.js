export const MENU_WINDOWS = ['WINDOW_MENU',"PICK AND PLAY"];
export const SELL_WINDOWS = ['SELL_WINDOWS',"SELL"];
export const SHOP_WINDOWS = ['SHOP_WINDOWS',"BUY"];
export const LOGIN_WINDOWS = ['LOGIN_WINDOWS',"CONNEXION"];
export const ERROR_WINDOWS = ['ERROR_WINDOWS',"ERREUR 404 "];
export const INSCRIPTION_WINDOWS = ['INSCRIPTION_WINDOWS',"INSCRIPTION"];
export const PLAY_WINDOWS = ['PLAY_WINDOWS',"PLAY"];
export const SET_WINDOWS = 'SET_WINDOWS'

export const setCurrentInternWindows = (windowsName)=>{
    return{
        type:SET_WINDOWS,
        payload:windowsName
    }
}