import {createStore} from '@reduxjs/toolkit'
import {globalReducer} from "./reducers/index";


const store = createStore(globalReducer);

export default store;