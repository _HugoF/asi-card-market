import { combineReducers } from 'redux';
import {navReducer} from "./navigatoryReducer";
import {cardReducer} from "./cardReducer";
import {userReducer} from "./userReducer";


// Merging several reducers in one
export const globalReducer = combineReducers({
    cardReducer: cardReducer,
    userReducer: userReducer,
    navigatorReducer : navReducer
});