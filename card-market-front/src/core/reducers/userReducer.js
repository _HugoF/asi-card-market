import {SET_USER} from "../actions/userActions";

const initialState ={
    userSelected:undefined
}

export const userReducer= (state = initialState, action) => {
    switch (action.type) {
        case SET_USER:
            return {
                ...state,
                userSelected: action.payload,
            };
        default:
            return state;
    }
}
