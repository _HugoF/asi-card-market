import {LOGIN_WINDOWS, SET_WINDOWS} from "../actions/navigatoryActions"

const initialState = {
    currentWindows : LOGIN_WINDOWS
}
export const navReducer = (state=initialState,action) => {
    switch (action.type){
        case SET_WINDOWS:
            return{
                ...state,
                currentWindows: action.payload,
            };
        default:
            return state;
    }
}