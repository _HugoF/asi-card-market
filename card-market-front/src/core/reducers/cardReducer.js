import {UPDATE_CARD, UPDATE_ID_CARD} from "../actions/cardActions";

const initialState ={
    cardSelected:{},
    selectedCardId:undefined
}

export const cardReducer= (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_ID_CARD:
            return {
                ...state,
                selectedCardId: action.payload,
            };
        case UPDATE_CARD:
            return {
                ...state,
                cardSelected: action.payload,
                selectedCardId: action.payload.id,
            };
        default:
            return state;
    }
}
