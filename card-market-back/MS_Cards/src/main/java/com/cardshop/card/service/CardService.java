package com.cardshop.card.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cardshop.card.mapper.CardMapper;
import com.cardshop.cardlib.dto.CardDTO;
import com.cardshop.card.repository.CardRepository;

@Service
public class CardService {
	
	@Autowired
	private CardRepository cRepository;
	
	@Autowired
	private CardBusWriterService cardBusWriter;

	private CardMapper cMapper = new CardMapper();

	public List<CardDTO> findAll() {
		return cRepository.findAll()
				.stream()
				.map(card -> cMapper.convertCardDto(card))
				.collect(Collectors.toList());
	}
	
	public CardDTO findById(int id) {
		Optional<CardDTO> cardOptional = cRepository.findById(id).map(card -> cMapper.convertCardDto(card));
		return cardOptional.isPresent() ? cardOptional.get(): null;
	}
	
	public List<CardDTO> findByOwner(int owner) {
		return cRepository.findByOwner(owner)
				.stream()
				.map(card -> cMapper.convertCardDto(card))
				.collect(Collectors.toList());
	}
	
	public void addCard(CardDTO card) {
		cardBusWriter.writeMessage("ADD_CARD", card);
	}
	
	/**
	 * Fonction appelée par le CardBusReaderService
	 * 
	 * @param card
	 */
	public void _addCard(CardDTO card) {
		cRepository.save(cMapper.convertCard(card));
	}
	
	public int updateOwner(int idCard, int idNewOwner) {
		cardBusWriter.writeMessage("UPDATE_OWNER", idCard, idNewOwner);
		return 1;
	}
	
	public int _updateOwner(int idCard, int idNewOwner) {
		return cRepository.updateOwner(idNewOwner, idCard);
	}
	
	public int updateMarket(int idCard, int idNewMarket) {
		cardBusWriter.writeMessage("UPDATE_MARKET", idCard, idNewMarket);
		return 1;
	}
	
	public int _updateMarket(int idCard, int idNewMarket) {
		return cRepository.updateMarket(idNewMarket, idCard);
	}
	
	public void deleteById(int idCard) {
		cardBusWriter.writeMessage("DELETE_CARD", idCard);
	}
	
	public void _deleteById(int idCard) {
		cRepository.deleteById(idCard);
	}

}
