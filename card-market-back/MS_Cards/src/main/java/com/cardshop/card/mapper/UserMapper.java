package com.cardshop.card.mapper;

import com.cardshop.userlib.dto.UserDTO;

public class UserMapper {

	public static UserDTO convertUserDto(UserDTO user) {
		return new UserDTO(user.getId(), user.getName(), user.getWallet(), user.getLogin(), user.getPassword());
	}
	
	public static UserDTO convertUser(UserDTO userDto) {
		return new UserDTO(userDto.getId(), userDto.getName(), userDto.getWallet(), userDto.getLogin(), userDto.getPassword());
	}
}
