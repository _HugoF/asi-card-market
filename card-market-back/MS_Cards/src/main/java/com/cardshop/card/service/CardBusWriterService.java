package com.cardshop.card.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.cardshop.cardlib.dto.CardDTO;

@Service
public class CardBusWriterService {
	
	/**
	 * Logger
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(CardBusWriterService.class);
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	public  void writeMessage(String topic, CardDTO message) {
		LOGGER.debug("[CardBusService] SEND a card to this topic: "+topic);
		jmsTemplate.convertAndSend(topic, message);
	}
	
	public  void writeMessage(String topic, int id) {
		LOGGER.debug("[CardBusService] SEND id to this topic: "+topic);
		jmsTemplate.convertAndSend(topic, id);
	}
	
	public  void writeMessage(String topic, int id1, int id2) {
		LOGGER.debug("[CardBusService] SEND ids to this topic: "+topic);
		jmsTemplate.convertAndSend(topic, id1+"|"+id2);
	}
	
}
