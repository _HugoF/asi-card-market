package com.cardshop.card.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cardshop.cardlib.dto.CardDTO;
import com.cardshop.cardlib.restclient.ICardRest;
import com.cardshop.card.service.CardService;

@RestController
@RequestMapping("/api")
public class CardController implements ICardRest {

	@Autowired
	CardService cService;

	@Override
	@RequestMapping(method=RequestMethod.GET, value="/cards")
	public List<CardDTO> findAll() {
		return cService.findAll();
	}

	@Override
	@RequestMapping(method=RequestMethod.GET, value="/card/{id}")
	public CardDTO findById(@PathVariable int id) {
		return cService.findById(id);
	}

	@Override
	@RequestMapping(method=RequestMethod.GET, value="/cards/{owner}")
	public List<CardDTO> findByOwner(@PathVariable int owner) {
		return cService.findByOwner(owner);
	}

	@Override
	@RequestMapping(method=RequestMethod.POST, value="/card")
	public void addCard(@RequestBody CardDTO card) {
		cService.addCard(card);
	}

	@Override
	@RequestMapping(method=RequestMethod.PUT, value="/card/{id}/owner/{idNewOwner}")
	public int updateOwner(@PathVariable int id, @PathVariable int idNewOwner) {
		return cService.updateOwner(id, idNewOwner);
	}

	@Override
	@RequestMapping(method=RequestMethod.PUT, value="/card/{id}/market/{idNewMarket}")
	public int updateMarket(@PathVariable int id, @PathVariable int idNewMarket) {
		return cService.updateMarket(id, idNewMarket);
	}

	@Override
	@RequestMapping(method=RequestMethod.DELETE, value="/card/{id}")
	public void deleteById(@PathVariable int id) {
		cService.deleteById(id);
	}

	

}
