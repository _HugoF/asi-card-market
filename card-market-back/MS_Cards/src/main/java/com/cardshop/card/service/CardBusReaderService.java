package com.cardshop.card.service;


import org.apache.activemq.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.cardshop.cardlib.dto.CardDTO;

@Component
public class CardBusReaderService {
	
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Autowired
	CardService cService;
	
	@JmsListener(destination = "ADD_CARD", containerFactory = "connectionFactory")
	public void receiveAddCard(CardDTO card, Message message) {
		cService._addCard(card);
	}
	
	@JmsListener(destination = "UPDATE_OWNER", containerFactory = "connectionFactory")
	public void receiveUpdateOwner(String msgStr, Message message) {
		String[] tab = msgStr.split("|");
		cService._updateOwner(Integer.parseInt(tab[0]), Integer.parseInt(tab[1]));
	}
	
	@JmsListener(destination = "UPDATE_MARKET", containerFactory = "connectionFactory")
	public void receiveUpdateMarket(String msgStr, Message message) {
		String[] tab = msgStr.split("|");
		cService._updateMarket(Integer.parseInt(tab[0]), Integer.parseInt(tab[1]));
	}
	
	@JmsListener(destination = "DELETE_CARD", containerFactory = "connectionFactory")
	public void receiveDelete(int idCard, Message message) {
		cService._deleteById(idCard);
	}
}
