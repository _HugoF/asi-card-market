package com.cardshop.card.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cardshop.card.model.Card;

public interface CardRepository extends JpaRepository<Card, Integer> {
	
	/**
	 * Trouve une carte en base selon son propriétaire
	 * 
	 * @param owner, l'id du propriétaire
	 * 
	 * @return <code>List<Card></code>
	 */
	public List<Card> findByOwner(int owner);
	
	@Modifying
	@Query(value = "UPDATE Card c SET c.owner=:idNewOwner WHERE c.id=:cardId")
	public int updateOwner(@Param("idNewOwner") int idNewOwner, @Param("cardId") int cardId);
	
	@Modifying
	@Query(value = "UPDATE Card c SET c.market=:idNewMarket WHERE c.id=:cardId")
	public int updateMarket(@Param("idNewMarket") int idNewMarket, @Param("cardId") int cardId);
}
