/*package com.cardshop.card.service;


import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.cardshop.card.model.Card;
import com.cardshop.card.repository.CardRepository;
import com.cardshop.cardlib.dto.CardDTO;
import com.cardshop.cardlib.enumeration.Affinity;
import com.cardshop.cardlib.enumeration.Family;

@ExtendWith(SpringExtension.class)
@WebMvcTest(value = CardService.class)
@AutoConfigureMockMvc(addFilters = false)
public class CardServiceTest {

	@Autowired
	private CardService cService;
	
	@MockBean
	private CardRepository cRepo;
	
	Card tmpCard = new Card(1, "name1", "description1", Family.ETOILE1, Affinity.BUG, "http://imgUrl1.com", "https://smallImgUrl1.com", 1, 2, 3, 4, 5, 0, 1);
	
	@Test
	public void testFindById() {
		Mockito.when(
				cRepo.findById(Mockito.any())
				).thenReturn(Optional.ofNullable(tmpCard));
		
		CardDTO CardInfos = cService.findById(45);
		
		String result = "Card ["+CardInfos.getId()+"]: name:"+CardInfos.getName();
		
		assertTrue(result.equals(tmpCard.toString()));
	}
	
	// add other test for each method
}*/
