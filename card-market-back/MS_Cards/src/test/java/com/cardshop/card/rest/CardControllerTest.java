/*package com.cardshop.card.rest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.cardshop.card.service.CardService;
import com.cardshop.cardlib.dto.CardDTO;
import com.cardshop.cardlib.enumeration.Affinity;
import com.cardshop.cardlib.enumeration.Family;

@ExtendWith(SpringExtension.class)
@WebMvcTest(value = CardController.class)
public class CardControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private CardService cService;
	
	CardDTO mockCard = new CardDTO(1, "name1", "description1", Family.ETOILE1, Affinity.BUG, "http://imgUrl1.com", "https://smallImgUrl1.com", 1, 2, 3, 4, 5, 0, 1);
	
	@Test
	  public void postTest() throws Exception {
	    String json = "{\"id\":1,\"name\":\"name1\",\"description\":\"description1\",\"family\":\"1 Etoile\",\"affinity\":\"Bug\",\"imgUrl\":\"http://imgUrl1.com\",\"smallImgUrl\":\"https://smallImgUrl1.com\",\"hp\":1,\"energy\":2,\"attack\":3,\"defense\":4,\"prix\":5,\"owner\":0,\"market\":1}";
	    
	    RequestBuilder requestBuilderAdd = MockMvcRequestBuilders.post("/api/card")
														.contentType(MediaType.APPLICATION_JSON)
	    												.content(json)
	    												.accept(MediaType.APPLICATION_JSON);
	    
	    mockMvc.perform(requestBuilderAdd).andReturn();
	  }
	
	@Test
	public void retrieveCard() throws Exception {	    
		Mockito.when(
				cService.findById(Mockito.anyInt())
				).thenReturn(mockCard);
		
		mockMvc.perform(MockMvcRequestBuilders.
		        get("/api/card/99")).
		        andExpect(MockMvcResultMatchers.status().isOk());
		
		mockMvc.perform(MockMvcRequestBuilders.
		        get("/api/card/1").accept(MediaType.APPLICATION_JSON)
		        ).
				andExpect(MockMvcResultMatchers.content().string("{\"id\":1,\"name\":\"name1\",\"description\":\"description1\",\"family\":\"ETOILE1\",\"affinity\":\"BUG\",\"imgUrl\":\"http://imgUrl1.com\",\"smallImgUrl\":\"https://smallImgUrl1.com\",\"hp\":1,\"energy\":2,\"attack\":3,\"defense\":4,\"prix\":5,\"owner\":0,\"market\":1}"));
	}

}*/
