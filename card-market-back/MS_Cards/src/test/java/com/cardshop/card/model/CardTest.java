package com.cardshop.card.model;


import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.cardshop.cardlib.enumeration.Affinity;
import com.cardshop.cardlib.enumeration.Family;



public class CardTest {

	private static List<String> stringList;
	private static List<Integer> intList;
	private static List<Family> famList;
	private static List<Affinity> affList;
	
	@BeforeAll
	public static void setUp() {
    	System.out.println("[BEFORE TEST] -- CardModel -- Add list informations ");
    	stringList = new ArrayList<String>();
		intList = new ArrayList<Integer>();
		famList = new ArrayList<Family>();
		affList = new ArrayList<Affinity>();
		stringList.add("normalString1");
		stringList.add("normalString2");
		stringList.add(";:!;!::!;;<>");
		intList.add(5);
		intList.add(500);
		intList.add(-1);
		famList.add(Family.ETOILE1);
		famList.add(Family.ETOILES2);
		famList.add(Family.ETOILES3);
		affList.add(Affinity.NORMAL);
		affList.add(Affinity.BUG);
		affList.add(Affinity.DARK);
    }
    
    @AfterAll
	public static void tearDown() {
    	System.out.println("[AFTER TEST] -- CLEAN hero list");
		stringList = null;
		intList = null;
		famList = null;
		affList = null;
    }
    
    @Test
	public void creatCard() {
    	System.out.println("[START] -- Card creation");
		for(String msg:stringList) {
			for(String msg2:stringList) {
				for(String msg3:stringList) {
					for(Integer msg4:intList) {
						for(Family msg5:famList) {
							for(Affinity msg6:affList) {
								/* int id, msg4
								 * String name, msg
								 * String description, msg2
								 * Family family, msg5
								 * Affinity affinity, msg6
								 * String imgUrl, msg3
								 * String smallImgUrl, msg
								 * int hp, msg4
								 * int energy, msg4
								 * int attack, msg4
								 * int defense, msg4
								 * int prix, msg4
								 * int owner, msg4
								 * int market msg4
								*/
								Card card = new Card(msg4, msg, msg2, msg5, msg6, msg3, msg, msg4, msg4, msg4, msg4, msg4, msg4, msg4);
								//System.out.println("msg:"+msg+", msg2:"+msg2+", msg3:"+msg3+", msg4:"+msg4+", msg5:"+msg5+", msg6:"+msg6);
								assertTrue(card.getId() == msg4);
								assertTrue(card.getName() == msg);
								assertTrue(card.getDescription() == msg2);
								assertTrue(card.getFamily() == msg5);
								assertTrue(card.getAffinity() == msg6);
								assertTrue(card.getImgUrl() == msg3);
								assertTrue(card.getSmallImgUrl() == msg);
								assertTrue(card.getHp() == msg4);
								assertTrue(card.getEnergy() == msg4);
								assertTrue(card.getAttack() == msg4);
								assertTrue(card.getDefense() == msg4);
								assertTrue(card.getPrix() == msg4);
								assertTrue(card.getOwner() == msg4);
								assertTrue(card.getMarket() == msg4);
							}
						}
					}	
				}	
			}
		}
		System.out.println("[END] -- Card Creation");
	}

    @Test
	public void diplayCard() {
    	System.out.println("[START] -- Display Card");
    	Card card = new Card(1, "name", "description", Family.ETOILE1, Affinity.BUG, "https://imgurl.com", "https://smallimgurl.com",1,2,3,4,5,0,1);
    	String expectedResult = "Card [1]: name:name";
    	System.out.println(expectedResult);
    	assertTrue(card.toString().equals(expectedResult));
    	System.out.println("[END] -- Display Card");
    }

}
