package com.cardshop.userlib.restclient;

import java.util.List;
import com.cardshop.userlib.dto.UserDTO;

/**
 * 
 * @author Hugo Ferrer
 *
 */

public interface IUserRest {
	
	/**
	 * Allow the client to save a user in the database
	 * 
	 * @param userDTO
	 */
	public void addUser(UserDTO userDTO);
	
	/**
	 * Allow the client to update a user in the database
	 * 
	 * @param userDTO
	 */
	public void updateUser(UserDTO userDTO);
	
	/**
	 * Allow the client to retrieve all the users in the database
	 * 
	 * @return A liste of <code>UserDTO</code>
	 */
	public List<UserDTO> findAll();
	
	/**
	 * Allow the client to retrieve a user by using its id
	 * 
	 * @param id
	 * 
	 * @return A <code>UserDTO</code>
	 */
	public UserDTO findById(int id);
	
	/**
	 * Allow the client to retrieve a user by using its login
	 * and its password.
	 * 
	 * @param login
	 * @param pwd
	 * 
	 * @return A <code>UserDTO</code>
	 */
	public UserDTO findByLoginAndPassword(String login, String pwd);
	
	/**
	 * Allow the client to retrieve a user by using its login
	 * 
	 * @param login
	 * 
	 * @return A <code>UserDTO</code>
	 */
	public UserDTO findByLogin(String login);
	
	/**
	 * Allow the client to update the wallet amount
	 * 
	 * @param id, the user's
	 * @param newWallet, the new wallet amount
	 */
	public void saveNewWallet(int id, int newWallet);
	
	/**
	 * Allow the client to delete a user in the database
	 * 
	 * @param id, the user's
	 */
	public void deleteUser(int id);
	
}
