package com.cardshop.userlib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CardshopUserLibApplication {

	public static void main(String[] args) {
		SpringApplication.run(CardshopUserLibApplication.class, args);
	}

}
