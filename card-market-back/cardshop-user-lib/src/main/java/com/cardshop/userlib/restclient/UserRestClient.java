package com.cardshop.userlib.restclient;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.cardshop.userlib.dto.UserDTO;

import reactor.core.publisher.Mono;

/**
 * 
 * Client REST pour les objets m�tiers utilisateurs
 *
 */

@Service
public class UserRestClient implements IUserRest {

	@Autowired
	@Qualifier("userWebClient")
	WebClient restClient;
	
	@Override
	public void saveNewWallet(int id, int newWallet) {
		restClient
		.post()
		.uri("/api/users/"+Integer.toString(id))
		.body(Mono.just(newWallet), Integer.class);
	}
	
	@Override
	public List<UserDTO> findAll() {
		return restClient
				.get()
				.uri("/api/users")
				.retrieve()
				.bodyToFlux(UserDTO.class)
				.collectList()
				.block();
	}

	@Override
	public UserDTO findById(int id) {
		return restClient
				.get()
				.uri("/api/users/"+Integer.toString(id))
				.retrieve()
				.bodyToMono(UserDTO.class)
				.block();
	}

	@Override
	public void addUser(UserDTO userDTO) {
		restClient
		.post()
		.uri("/api/users")
		.body(Mono.just(userDTO), UserDTO.class);
	}

	@Override
	public void updateUser(UserDTO userDTO) {
		restClient
		.put()
		.uri("/api/user")
		.body(Mono.just(userDTO), UserDTO.class);
	}

	@Override
	public UserDTO findByLoginAndPassword(String login, String pwd) {
		return restClient
				.get()
				.uri("/api/user/"+login+"/"+pwd)
				.retrieve()
				.bodyToMono(UserDTO.class)
				.block();
	}

	@Override
	public void deleteUser(int id) {
		restClient
		.delete()
		.uri("/api/users/"+Integer.toString(id));
	}

	@Override
	public UserDTO findByLogin(String login) {
		return restClient
				.get()
				.uri("/api/user/"+login)
				.retrieve()
				.bodyToMono(UserDTO.class)
				.block();
	}
	
}
