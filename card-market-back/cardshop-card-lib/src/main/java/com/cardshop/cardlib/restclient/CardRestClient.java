package com.cardshop.cardlib.restclient;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.cardshop.cardlib.dto.CardDTO;

import reactor.core.publisher.Mono;

@Service
public class CardRestClient implements ICardRest {

	@Autowired
	@Qualifier("cardWebClient")
	WebClient restClient;

	@Override
	public List<CardDTO> findAll() {
		return restClient
				.get()
				.uri("/api/cards")
				.retrieve()
				.bodyToFlux(CardDTO.class)
				.collectList()
				.block();
	}
	
	@Override
	public CardDTO findById(int id) {
		return restClient
				.get()
				.uri("/api/card/"+Integer.toString(id))
				.retrieve()
				.bodyToMono(CardDTO.class)
				.block();
	}
	
	@Override
	public List<CardDTO> findByOwner(int owner) {
		return restClient
				.get()
				.uri("/api/cards/"+Integer.toString(owner))
				.retrieve()
				.bodyToFlux(CardDTO.class)
				.collectList()
				.block();
	}
	
	@Override
	public void addCard(CardDTO card) {
		restClient
			.post()
			.uri("/api/cards")
			.body(Mono.just(card), CardDTO.class);
	}

	@Override
	public int updateOwner(int idCard, int idNewOwner) {
		return restClient
			.put()
			.uri("/api/card/"+Integer.toString(idCard)+"/owner/"+Integer.toString(idNewOwner))
			.retrieve()
			.bodyToMono(Integer.class)
			.block();
	}

	@Override
	public int updateMarket(int idCard, int idNewMarket) {
		return restClient
				.put()
				.uri("/api/card/"+Integer.toString(idCard)+"/market/"+Integer.toString(idNewMarket))
				.retrieve()
				.bodyToMono(Integer.class)
				.block();
	}

	@Override
	public void deleteById(int idCard) {
		restClient.delete().uri("/api/card/"+Integer.toString(idCard));
	}
	
}
