package com.cardshop.cardlib.restclient;

import java.util.List;

import com.cardshop.cardlib.dto.CardDTO;

public interface ICardRest {
	
	/**
	 * Permet de r�cup�rer toutes les cartes
	 * 
	 * @return <code>List<CardDTO></code>
	 */
	List<CardDTO> findAll();
	
	/**
	 * R�cup�re une carte selon son id
	 * 
	 * @param id
	 * @return CardDTO
	 */
	CardDTO findById(int id);
	
	/**
	 * Permet de r�cup�rer les cartes d'un utilisateur
	 * 
	 * @param idOwner, l'id de l'utilisateur
	 * 
	 * @return <code>List<CardDTO></code>
	 */
	List<CardDTO> findByOwner(int idOwner);
	
	/**
	 * Permet d'ajouter une carte
	 * 
	 * @param card
	 */
	void addCard(CardDTO card);
	
	/**
	 * Permet de mettre � jour le propri�taire de la carte
	 * 
	 * @param idCard, id de la carte
	 * @param idNewOwner, l'id utilisateur du nouveau propri�taire
	 * 
	 * @return le nombre de lignes affect�es
	 */
	int updateOwner(int idCard, int idNewOwner);
	
	/**
	 * Permet de mettre � jour le market d'une carte
	 * 
	 * @param idCard, l'id de la carte
	 * @param idNewMarket, l'id market du nouveau market
	 * 
	 * @return le nombre de lignes affect�es
	 */
	int updateMarket(int idCard, int idNewMarket);
	
	/**
	 * Permet de supprimer une carte selon l'id
	 * 
	 * @param idCard, l'id de la carte � supprimer
	 */
	void deleteById(int idCard);
	
}
