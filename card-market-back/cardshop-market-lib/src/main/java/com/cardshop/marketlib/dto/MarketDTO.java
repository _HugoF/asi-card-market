package com.cardshop.marketlib.dto;

/**
 * Data Transfer Object of the Market model
 */
public class MarketDTO {
	
	/**
	 * Id du march�
	 */
	private int id;
	
	/**
	 * Id de l'utilisateur qui ach�te
	 */
	private int idBuyer;
	
	/**
	 * Id de l'utilisateur qui vend
	 */
	private int idSeller;
	
	/**
	 * Id de la carte qui fait l'objet du march�
	 */
	private int idCard;
	
	/**
	 * Constructeur par d�faut
	 */
	public MarketDTO() {}
	
	/**
	 * Constructeur avec param�tres
	 * @param id
	 * @param idBuyer
	 * @param idSeller
	 * @param idCard
	 */
	public MarketDTO(int id, int idBuyer, int idSeller, int idCard) {
		super();
		this.id = id;
		this.idBuyer = idBuyer;
		this.idSeller = idSeller;
		this.idCard = idCard;
	}
	
	/**
	 * Getter de l'attribut id
	 * 
	 * @return id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Setter de l'attribut id
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Getter de l'attribut idBuyer
	 * 
	 * @return idBuyer
	 */
	public int getIdBuyer() {
		return idBuyer;
	}
	
	/**
	 * Setter de l'attribut idBuyer
	 * 
	 * @param idBuyer
	 */
	public void setIdBuyer(int idBuyer) {
		this.idBuyer = idBuyer;
	}
	
	/**
	 * Getter de l'attribut idSeller
	 * 
	 * @return
	 */
	public int getIdSeller() {
		return idSeller;
	}
	
	/**
	 * Setter de l'attribut idSeller
	 * 
	 * @param idSeller
	 */
	public void setIdSeller(int idSeller) {
		this.idSeller = idSeller;
	}
	
	/**
	 * Getter de l'attribut idCard
	 * 
	 * @return idCard
	 */
	public int getIdCard() {
		return idCard;
	}
	
	/**
	 * Setter de l'attribut idCard
	 * 
	 * @param idCard
	 */
	public void setIdCard(int idCard) {
		this.idCard = idCard;
	}

	@Override
	public String toString() {
		return "MarketDTO [id=" + id + ", idBuyer=" + idBuyer + ", idSeller=" + idSeller + ", idCard=" + idCard + "]";
	}
}
