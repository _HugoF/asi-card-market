package com.cardshop.marketlib.dto;

public class ExchangeDTO {
	
	private int userId;
	private int cardId;
	
	public ExchangeDTO() {
	}

	public ExchangeDTO(int userId, int cardId) {
		super();
		this.userId = userId;
		this.cardId = cardId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getCardId() {
		return cardId;
	}

	public void setCardId(int cardId) {
		this.cardId = cardId;
	}

	@Override
	public String toString() {
		return "ExchangeDTO [userId=" + userId + ", cardId=" + cardId + "]";
	}
}