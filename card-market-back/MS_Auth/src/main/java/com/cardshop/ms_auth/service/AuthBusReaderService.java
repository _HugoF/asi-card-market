package com.cardshop.ms_auth.service;

import org.apache.activemq.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.cardshop.authlib.dto.TokenDTO;

@Component
public class AuthBusReaderService {
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Autowired
	AuthService aService;
	
	@JmsListener(destination = "ADD_TOKEN", containerFactory = "connectionFactory")
	public void receiveSellCard(TokenDTO t, Message message) {
		aService._addToken(t);
	}
}
