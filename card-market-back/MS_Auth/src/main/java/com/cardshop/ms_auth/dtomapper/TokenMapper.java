package com.cardshop.ms_auth.dtomapper;

import com.cardshop.authlib.dto.TokenDTO;
import com.cardshop.ms_auth.model.Token;

public class TokenMapper {

	public static TokenDTO convertTokenDto(Token token) {
		return new TokenDTO(token.getId(),token.getToken(),token.getUserId());
	}
	
	public static Token convertDtoToken(TokenDTO tokenDto) {
		return new Token(tokenDto.getId(),tokenDto.getToken(),tokenDto.getUserId());
	}
}
