package com.cardshop.ms_auth.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity(name="token")
public class Token {
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="userseq")
	@SequenceGenerator(name = "tokenseq", sequenceName = "tokenseq", allocationSize = 1)
	@Column(name="TOKEN_ID", unique=true, nullable=false)
	private int id;
	private String token;
	private int userId;
	public Token() {
	
	}
	public Token(int id,String token,int userId) {
		super();
		this.id = id;
		this.token = token;
		this.userId = userId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
}
