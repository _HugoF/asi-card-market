package com.cardshop.ms_auth.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.cardshop.authlib.dto.TokenDTO;

@Service
public class AuthBusWriterService {

	/**
	 * Logger
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(AuthBusWriterService.class);
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	public  void writeMessage(String topic,TokenDTO t) {
		LOGGER.debug("[CardBusService] SEND ids to this topic: "+topic);
		jmsTemplate.convertAndSend(topic, t);
	}
}
