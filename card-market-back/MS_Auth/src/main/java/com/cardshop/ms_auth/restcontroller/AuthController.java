package com.cardshop.ms_auth.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.cardshop.authlib.dto.TokenDTO;
import com.cardshop.authlib.restclient.IAuthRest;
import com.cardshop.ms_auth.service.AuthService;
import com.cardshop.userlib.dto.UserDTO;

@RestController
public class AuthController implements IAuthRest {

	@Autowired
	AuthService aService;
	
	@Override
	@RequestMapping(method=RequestMethod.POST,value="/create")
    public String createToken(@RequestBody String login) {
    	UserDTO userDto = aService.getUserByLogin(login);
    	if (userDto != null){
    		return aService.createToken(userDto);
    	}else {
    		return "";
    	}	 
    }
	
	@Override
	@RequestMapping(method=RequestMethod.POST,value="/check")
    public void checkToken(@RequestHeader("encoded-token") String encodedToken) {
        if(aService.checkToken(encodedToken) == false) {
    	   throw new ResponseStatusException(HttpStatus.NOT_FOUND,"Token not found");
        }
    }
	
	@Override
    @RequestMapping(method=RequestMethod.POST,value="/logout")
    public void disconnectUser(@RequestHeader("encoded-token") String encodedToken) {
    	TokenDTO tokenDto = new TokenDTO();
    	tokenDto.setToken(encodedToken);
        aService.eraseToken(tokenDto);
    }
	
}
