package com.cardshop.ms_auth.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.cardshop.ms_auth.model.Token;


public interface AuthRepository extends CrudRepository<Token, Integer> {
	
	public Optional<Token> findByToken(String token);
	
	public Optional<Token> findById(int id);
	
	public Optional<Token> findByUserId(int userId);
	
	public int deleteByToken(String tocken);
	@Modifying
	@Query(value = "UPDATE token t SET t.token=:newToken WHERE t.id=:tokenId")
	public void saveNewToken(@Param("tokenId") int tokenId, @Param("newToken")  int newToken);
}
