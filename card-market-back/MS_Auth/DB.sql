CREATE DATABASE auth;
CREATE USER auth with encrypted password 'password';
GRANT ALL PRIVILEGES ON DATABASE auth to auth;