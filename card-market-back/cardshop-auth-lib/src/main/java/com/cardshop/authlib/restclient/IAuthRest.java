package com.cardshop.authlib.restclient;

/**
 * Comportement d'un controlleur REST pour l'authentification
 */
public interface IAuthRest {
	/**
	 * Cr�e un token
	 * 
	 * @param login
	 * @return
	 */
	public String createToken(String login);
	
	/**
	 * V�rifie le token
	 * @param encodedToken
	 */
	public void checkToken(String encodedToken);
	
	/**
	 * D�connecte l'utilisateur
	 * @param encodedToken
	 */
	public void disconnectUser(String encodedToken);
}
