package com.cardshop.authlib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CardshopAuthLibApplication {

	public static void main(String[] args) {
		SpringApplication.run(CardshopAuthLibApplication.class, args);
	}

}
