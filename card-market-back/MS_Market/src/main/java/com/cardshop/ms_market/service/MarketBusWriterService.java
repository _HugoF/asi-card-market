package com.cardshop.ms_market.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class MarketBusWriterService {
	
	/**
	 * Logger
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(MarketBusWriterService.class);
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	public  void writeMessage(String topic, int id1, int id2) {
		LOGGER.debug("[CardBusService] SEND ids to this topic: "+topic);
		jmsTemplate.convertAndSend(topic, id1+"|"+id2);
	}
}
