package com.cardshop.ms_market.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Cette classe représente l'entité Market en base de données
 */
@Entity
public class Market {
	
	/**
	 * Id de l'utilisateur
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	/**
	 * Id de l'utilisateur qui a acheter
	 */
	private int idBuyer;
	
	/**
	 * Id de l'utilisateur qui a vendu
	 */
	private int idSeller;
	
	/**
	 * Id de la carte qui a fait l'objet du marché
	 */
	private int idCard;
	
	/**
	 * Constructeur par défaut
	 */
	public Market() {}
	
	/**
	 * Constructeur avec paramètres
	 * @param id, id du marché
	 * @param idBuyer, id de l'utilisateur qui a acheté
	 * @param idSeller, id de l'utilisateur qui a vendu
	 * @param idCard, id de la carte qui a fait l'objet du marché
	 */
	public Market(int id, int idBuyer, int idSeller, int idCard) {
		super();
		this.id = id;
		this.idBuyer = idBuyer;
		this.idSeller = idSeller;
		this.idCard = idCard;
	}
	
	/**
	 * Getter de l'attribut id
	 * 
	 * @return id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Setter de l'attribut id
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Getter de l'attribut idBuyer
	 * 
	 * @return idBuyer
	 */
	public int getIdBuyer() {
		return idBuyer;
	}
	
	/**
	 * Setter de l'attribut idBoyer
	 * 
	 * @param id_buyer
	 */
	public void setIdBuyer(int idBuyer) {
		this.idBuyer = idBuyer;
	}
	
	/**
	 * Getter de l'attribut idSeller
	 * 
	 * @return idSeller
	 */
	public int getIdSeller() {
		return idSeller;
	}
	
	/**
	 * Setter de l'attribut idSeller
	 * 
	 * @param idSeller
	 */
	public void setIdSeller(int idSeller) {
		this.idSeller = idSeller;
	}
	
	/**
	 * Getter de l'attribut idCard
	 * 
	 * @return idCard
	 */
	public int getIdCard() {
		return idCard;
	}
	
	/**
	 * Setter de l'attribut idCard
	 * 
	 * @param id_card
	 */
	public void setIdCard(int idCard) {
		this.idCard = idCard;
	}
	
	@Override
	public String toString() {
		return "Market [id=" + id + ", id_buyer=" + idBuyer + ", id_seller=" + idSeller + ", id_card=" + idCard+ "]";
	}
	
	
}
