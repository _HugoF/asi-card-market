package com.cardshop.ms_market.dtomapper;

import com.cardshop.marketlib.dto.MarketDTO;
import com.cardshop.ms_market.model.Market;

public class MarketDTOMapper {
	
	public MarketDTOMapper() {}
	
	public Market getMarket(MarketDTO marketDTO) {
		return new Market(marketDTO.getId(), marketDTO.getIdBuyer(), marketDTO.getIdSeller(), marketDTO.getIdCard());
	}
	
	public MarketDTO getMarketDTO(Market market) {
		return new MarketDTO(market.getId(), market.getIdBuyer(), market.getIdSeller(), market.getIdCard());
	}
}
