package com.cardshop.ms_market.service;

import org.apache.activemq.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;


@Component
public class MarketBusReaderService {
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Autowired
	MarketService mService;
	
	@JmsListener(destination = "Sell_Card", containerFactory = "connectionFactory")
	public void receiveSellCard(String msgStr, Message message) {
		String[] tab = msgStr.split("|");
		mService._sellCard(Integer.parseInt(tab[0]), Integer.parseInt(tab[1]));
	}
	
	@JmsListener(destination = "Buy_Card", containerFactory = "connectionFactory")
	public void receiveBuyCard(String msgStr, Message message) {
		String[] tab = msgStr.split("|");
		mService._buyCard(Integer.parseInt(tab[0]), Integer.parseInt(tab[1]));
	}
}
