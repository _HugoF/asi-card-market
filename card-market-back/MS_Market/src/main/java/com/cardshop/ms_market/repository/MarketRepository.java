package com.cardshop.ms_market.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cardshop.ms_market.model.Market;

public interface MarketRepository extends JpaRepository<Market, Integer> {
	
	public Market findById(int id);
	
}
