package com.cardshop.ms_user.model;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class UserTest {
	
	@Test
	public void createUser() {
		
		int id = 1;
		String name = "John Doe";
		int wallet = 100;
		String login = "j.doe";
		String password = "M0t_D€_P@$s3";
		
		User user = new User(id, name, wallet, login, password);
		
		System.out.println(
			"id: "+id
			+", name: "+name
			+", wallet: "+wallet
			+", login: "+login
			+", password: "+password
		);
		
		assertTrue(id == user.getId());
		assertTrue(name == user.getName());
		assertTrue(wallet == user.getWallet());
		assertTrue(login == user.getLogin());
		assertTrue(password == user.getPassword());
		
	}
	
}
