package com.cardshop.ms_user.service;

import org.apache.activemq.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.cardshop.userlib.dto.UserDTO;

@Component
public class UserBusReaderService {
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Autowired
	UserService uService;
	
	@JmsListener(destination = "ADD_USER", containerFactory = "connectionFactory")
	public void receiveSellCard(UserDTO user, Message message) {
		uService._addUser(user);
	}
	
	@JmsListener(destination = "UPDATE_USER", containerFactory = "connectionFactory")
	public void receiveUpdateUser(UserDTO user, Message message) {
		uService._updateUser(user);
	}
	
	@JmsListener(destination = "SAVE_WALLET", containerFactory = "connectionFactory")
	public void receiveUpdateUser(String msgStr, Message message) {
		String[] tab = msgStr.split("|");
		uService._saveNewWallet(Integer.parseInt(tab[0]), Integer.parseInt(tab[1]));
	}
	
	@JmsListener(destination = "DELETE_USER", containerFactory = "connectionFactory")
	public void receiveUpdateUser(int id, Message message) {
		uService._deleteUser(id);
	}
}
