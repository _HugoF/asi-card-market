package com.cardshop.ms_user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.cardshop.ms_user.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	
	/**
	 * Trouve un utilisateur avec son id
	 * 
	 * @param id
	 * @return Un utilisateur
	 */
	public User findById(int id);
	
	/**
	 * Trouve un utilisateur en utilisant son login
	 * et son mot de passe
	 * 
	 * @param login
	 * @param pwd
	 * @return Un utilisateur
	 */
	public User findByLoginAndPassword(String login,String pwd);
	
	/**
	 * Trouve un utilisateur en utilisant son login
	 * 
	 * @param login
	 * 
	 * @return Un utilisateur
	 */
	public User findByLogin(String login);
	
}
