package com.cardshop.ms_user.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.cardshop.userlib.dto.UserDTO;

@Service
public class UserBusWriterService {
	
	/**
	 * Logger
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(UserBusWriterService.class);
	
	@Autowired
	private JmsTemplate jmsTemplate;
	
	public  void writeMessage(String topic, int id) {
		LOGGER.debug("[CardBusService] SEND ids to this topic: "+topic);
		jmsTemplate.convertAndSend(topic, id);
	}
	
	public  void writeMessage(String topic, int id1, int id2) {
		LOGGER.debug("[CardBusService] SEND ids to this topic: "+topic);
		jmsTemplate.convertAndSend(topic, id1+"|"+id2);
	}
	
	public  void writeMessage(String topic, UserDTO user) {
		LOGGER.debug("[CardBusService] SEND ids to this topic: "+topic);
		jmsTemplate.convertAndSend(topic, user);
	}
}
