package com.cardshop.ms_user.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cardshop.ms_user.dtomapper.UserDTOMapper;
import com.cardshop.ms_user.repository.UserRepository;
import com.cardshop.userlib.dto.UserDTO;

/**
 * 
 * Service de gestion d'un User
 *
 */
@Service
public class UserService {

	/**
	 * Injection du UserRepository
	 */
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	private UserBusWriterService userBusWriter;
	
	/**
	 * Mapper User <=> UserDTO
	 */
	private UserDTOMapper userDTOMapper;
	
	/**
	 * Constructor of UserService
	 */
	public UserService() {
		userDTOMapper = new UserDTOMapper();
	}
	
	/**
	 * Allow the client to save a user in the database
	 * 
	 * @param userDTO
	 */
	public void addUser(UserDTO userDTO) {
		userBusWriter.writeMessage("ADD_USER", userDTO);
	}
	
	/**
	 * Allow the client to save a user in the database
	 * 
	 * @param userDTO
	 */
	public void _addUser(UserDTO userDTO) {
		userRepository.save(userDTOMapper.getUser(userDTO));
	}
	
	/**
	 * Allow the client to update a user in the database
	 * 
	 * @param userDTO
	 */
	public void updateUser(UserDTO userDTO) {
		userBusWriter.writeMessage("UPDATE_USER", userDTO);
	}
	
	public void _updateUser(UserDTO userDTO) {
		userRepository.save(userDTOMapper.getUser(userDTO));
	}
	
	/**
	 * Allow the client to retrieve all the users in the database
	 * 
	 * @return A liste of <code>UserDTO</code>
	 */
	public List<UserDTO> findAll() {
		return userRepository.findAll()
				.stream()
				.map(user -> userDTOMapper.getUserDTO(user))
				.collect(Collectors.toList());
	}
	
	/**
	 * Allow the client to retrieve a user by using its id
	 * 
	 * @param id
	 * 
	 * @return A <code>UserDTO</code>
	 */
	public UserDTO findById(int id) {
		return userDTOMapper.getUserDTO(userRepository.findById(id));
	}
	
	/**
	 * Allow the client to retrieve a user by using its login
	 * and its password.
	 * 
	 * @param login
	 * @param pwd
	 * 
	 * @return A <code>UserDTO</code>
	 */
	public UserDTO findByLoginAndPassword(String login, String pwd) {
		return userDTOMapper.getUserDTO(userRepository.findByLoginAndPassword(login, pwd));
	}
	
	public UserDTO findByLogin(String login) {
		return userDTOMapper.getUserDTO(userRepository.findByLogin(login));
	}
	
	/**
	 * Allow the client to update the wallet amount
	 * 
	 * @param id, the user's
	 * @param newWallet, the new wallet amount
	 */
	public void saveNewWallet(int id, int newWallet) {
		userBusWriter.writeMessage("SAVE_WALLET", id, newWallet);
	}
	
	public void _saveNewWallet(int id, int newWallet) {
		UserDTO userDTO = findById(id);
		userDTO.setWallet(newWallet);
		userRepository.save(userDTOMapper.getUser(userDTO));
	}
	
	/**
	 * Allow the client to delete a user in the database
	 * 
	 * @param id, the user's
	 */
	public void deleteUser(int id) {
		userBusWriter.writeMessage("DELETE_USER", id);
	}
	
	public void _deleteUser(int id) {
		userRepository.deleteById(id);
	}
	
}
