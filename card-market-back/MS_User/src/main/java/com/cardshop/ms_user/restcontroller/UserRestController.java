package com.cardshop.ms_user.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cardshop.ms_user.service.UserService;
import com.cardshop.userlib.dto.UserDTO;
import com.cardshop.userlib.restclient.IUserRest;

/**
 * 
 * Controlleur de gestion des utilisateurs
 *
 */

@RestController
public class UserRestController implements IUserRest {

	@Autowired
	UserService userService;
	
	@Override
	@RequestMapping(method=RequestMethod.POST, value="/api/user")
	public void addUser(@RequestBody UserDTO userDTO) {
		userService.addUser(userDTO);
	}
	
	@Override
	@RequestMapping(method=RequestMethod.POST, value="/api/user/{id}/wallet")
	public void saveNewWallet(@PathVariable int id, @RequestBody int newWallet) {
		userService.saveNewWallet(id, newWallet);
	}
	
	@Override
	@RequestMapping(method=RequestMethod.GET, value="/api/users")
	public List<UserDTO> findAll() {
		return userService.findAll();
	}

	@Override
	@RequestMapping(method=RequestMethod.GET, value="/api/user/{id}")
	public UserDTO findById(@PathVariable int id) {
		return userService.findById(id);
	}

	@Override
	@RequestMapping(method=RequestMethod.GET, value="/api/user/{login}/{password}")
	public UserDTO findByLoginAndPassword(@PathVariable String login, @PathVariable String password) {
		return userService.findByLoginAndPassword(login, password);
	}

	@Override
	@RequestMapping(method=RequestMethod.PUT, value="/api/user")
	public void updateUser(UserDTO userDTO) {
		userService.updateUser(userDTO);
	}

	@Override
	@RequestMapping(method=RequestMethod.DELETE, value="/api/user/{id}")
	public void deleteUser(@PathVariable int id) {
		userService.deleteUser(id);
	}

	@Override
	@RequestMapping(method=RequestMethod.GET, value="/api/user/{login}")
	public UserDTO findByLogin(String login) {
		return userService.findByLogin(login);
	}
	
}
