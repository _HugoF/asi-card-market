package com.cardshop.ms_user.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 
 * Représente la table d'un utilisateur en base de données
 *
 */

@Entity
@Table(name = "users")
public class User {
	
	/**
	 * Id d'un enregistrement d'un utilisateur
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="userseq")
	@SequenceGenerator(name = "userseq", sequenceName = "userseq", allocationSize = 1)
	private int id;
	
	/**
	 * Nom de l'utilisateur
	 */
	private String name;
	
	/**
	 * Solde du portefeuille de l'utilisateur
	 */
	private int wallet;
	
	/**
	 * Login de l'utilisateur
	 */
	private String login;
	
	/**
	 * Mot de passe de l'utilisateur
	 */
	private String password;
	
	/**
	 * Constructeur par défaut
	 */
	public User() {
		
	}
	
	/**
	 * Constructeur avec paramètres
	 * 
	 * @param id
	 * @param name
	 * @param wallet
	 * @param login
	 * @param password
	 */
	public User(int id, String name, int wallet, String login, String password) {
		super();
		this.id = id;
		this.name = name;
		this.wallet = wallet;
		this.login = login;
		this.password = password;
	}
	
	/**
	 * Getters et Setters des attributs
	 */
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getWallet() {
		return wallet;
	}
	
	public void setWallet(int wallet) {
		this.wallet = wallet;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

}
